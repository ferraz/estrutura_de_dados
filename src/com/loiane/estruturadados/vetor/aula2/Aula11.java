package com.loiane.estruturadados.vetor.aula2;

import com.loiane.estruturadados.vetor.Lista;
import com.loiane.estruturadados.vetor.VetorObjetos;

public class Aula11 {

	public static void main(String[] args) {
		
		Contato c1 = new Contato("Contato 1", "1234-4567", "contato1@email.com");
		Contato c2 = new Contato("Contato 2", "4434-4667", "contato2@email.com");
		Contato c3 = new Contato("Contato 3", "3534-4567", "contato3@email.com");
		Contato c4 = new Contato("Contato 4", "3534-4587", "contato4@email.com");
		
		Lista<Contato> vetor = new Lista(1);
		vetor.adiciona(c1);
	}

}
