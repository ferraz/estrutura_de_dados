package com.loiane.estruturadados.vetor.aula2;

import com.loiane.estruturadados.vetor.Vetor;

public class Aula8 {

	public static void main(String[] args) {
		
		
		Vetor vetor = new Vetor(3);
		vetor.adiciona("B");
		vetor.adiciona("C");
		vetor.adiciona("E");
		vetor.adiciona("F");
		vetor.adiciona("G");
		
		System.out.println(vetor);
		
	}

}
